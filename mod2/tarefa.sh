#!/usr/bin/env bash
#
# tarefa.sh - Imprime uma sequência de asteriscos(*)
#
# Site:       https://4fasters.com.br
# Autor:      Mateus Müller
# Manutenção: Wiris Rafael Januario Wernek
#
# ------------------------------------------------------------------------ #
#  Este programa irá imprimir no shell uma sequencia decrescente de asteriscos a cada linha
#
#  Exemplos:
#      $ ./tarefa.sh
#      Neste exemplo o script será executado com valores padrões inicio em 0 e máximo 100
#
# ------------------------------------------------------------------------ #
# Histórico:
#
#   v1.0 03/10/2018, Mateus:
#       - Início do programa
#       - Conta com a funcionalidade da impressão decrescente
#   v1.1 06/11/2021, Wiris:
#       - Alterada identação
#       - Adicionado cabeçalho[
#
# ------------------------------------------------------------------------ #
# Testado em:
#   bash 5.1.4
#   zsh  5.8
#
# ------------------------------------------------------------------------ #
#
# ------------------------------- VARIÁVEIS ----------------------------------------- #

INICIO=0
MAXIMO=100

# ------------------------------------------------------------------------ #

# ------------------------------- TESTES ----------------------------------------- #

[ $INICIO -ge $MAXIMO ] && exit 1

# ------------------------------------------------------------------------ #

# ------------------------------- EXECUÇÃO ----------------------------------------- #

for i in $(seq $INICIO $MAXIMO)
    do
        for j in $(seq $i $MAXIMO)
            do printf "*"
        done
    printf "\n"
done

# ------------------------------------------------------------------------ #
