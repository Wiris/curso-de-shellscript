#!/usr/bin/env bash

for (( i = 0; i <= 10; i++ ))
do
  [ $(( $i % 2)) = 0 ] && echo "Valor $i é divisível por 2 com resultado $(( $i / 2)) e resto igual à $(( $i % 2))"
done
