#!/usr/bin/env bash

NOME="Wiris Rafael"

echo "$NOME"

NUMERO1=24
NUMERO2=54

TOTAL=$((NUMERO1+NUMERO2))
echo "$TOTAL"

SAIDA_CAT="$(cat /etc/passwd | grep wiris)"

echo "$SAIDA_CAT"

echo "------------------------------"
echo "Parametro 1: $1"
echo "Parametro 2: $2"
echo "Todos os parametros: $*"
echo "Quantos parametros: $#"
echo "Saida do ultimo comando: $?"
echo "PID: $$"
echo "Nome do Script: $0"
