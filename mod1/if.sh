#!/usr/bin/env bash
VAR1=""
VAR2=""
VAR3="teste"

if [[ "$VAR1" = "$VAR2" ]]; then
  echo "São iguais"
fi

if [[ "$VAR1" = "$VAR3" ]]
then
  echo "São iguais"
fi

if test "$VAR1" = "$VAR2"
then
  echo "São iguais"
fi

if [ "$VAR1" = "$VAR2" ]
then
  echo "São iguais"
fi

# Forma mais recomendada
[ "$VAR1" = "$VAR2" ] && echo "São iguais"
