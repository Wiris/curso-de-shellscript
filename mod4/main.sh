#!/usr/bin/env bash
#
# main.sh - Gerência seus alias
#
# Autor:      Wiris Rafael Januario Wernek
# Manutenção: Wiris Rafael Januario Wernek
#
# ------------------------------------------------------------------------ #
#  Este programa irá criar, buscar, modificar e remover alias de uma forma simplificada
#
#  Exemplos:
#      $ ./main.sh -l
#      Neste exemplo todos os alias cadastrados pelo script serão listados 
#
# ------------------------------------------------------------------------ #
# Histórico:
#   v1.0 17/11/2021 Wiris
#      - Adicionado -h, -v, -r, -l, -s & -e
#   v1.0 25/11/2021 Wiris
#      - Adicionado -d
#
# ------------------------------------------------------------------------ #
# Testado em:
#   bash 5.1.4
#   zsh  5.8
#
# ------------------------------------------------------------------------ #
#
# ------------------------------- VARIÁVEIS ----------------------------------------- #
MENSAGEM_USO="
    $(basename $0) - [OPÇÕES]

    -h - Menu de Ajuda
    -v - Exibe a Versão
    -s - Realiza como soma     
    -l - Realiza como multiplicação
    -d - Debugar o Script                
"
VERSAO="v1.0"
CHAVE_DEBUG=0
NIVEL_DEBUG=0
OPERACAO=""

# ------------------------------------------------------------------------ #

# ------------------------------- TESTES ----------------------------------------- #

# ------------------------------------------------------------------------ #

# ------------------------------- FUNÇÕES ----------------------------------------- #
[[ $1 -eq "-s" || $2 -eq "-s" ]] && OPERACAO="Soma"
[[ $1 -eq "-l" || $2 -eq "-l" ]] && OPERACAO="Subtração"

Debugar () {
  [ $1 -le $NIVEL_DEBUG ] && echo "Debug $* ------"
}

Calculo () {
  local total=0

  for i in $(seq 1 25); do
      Debugar 1 "Entrei no for com o valor: $i"
      total=$(($total+$i))
      Debugar 2 "Depois da $OPERACAO: $total"
  done
}
# ------------------------------------------------------------------------ #
