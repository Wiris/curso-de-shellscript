#!/usr/bin/env bash
#
# chaves_flags.sh - Extrai usuários do /etc/passwd
#
# Autor:      Wiris Rafael Januario Wernek
# Manutenção: Wiris Rafael Januario Wernek
#
# ------------------------------------------------------------------------ #
#  Este programa irá imprimir no shell uma sequencia decrescente de asteriscos a cada linha
#
#  Exemplos:
#      $ ./chaves_flags.sh -s -m
#      Neste exemplo ficará em maiúsculo e ordem alfabética
#
# ------------------------------------------------------------------------ #
# Histórico:
#   v1.0 17/11/2021 Wiris
#      - Adicionado -s, -v & -h
#   v1.1 17/11/2021 Wiris
#      - Trocado IF por Case
#      - Adicionado basename
#   v1.2 17/11/2021 Wiris
#      - Adicionado -m
#      - Adicionado 2 flags
#   v1.3 17/11/2021 Wiris
#      - Adicionado While com shift e teste de variável
#      - Adicionado 2 flags
#
#
# ------------------------------------------------------------------------ #
# Testado em:
#   bash 5.1.4
#   zsh  5.8
#
# ------------------------------------------------------------------------ #
#
# ------------------------------- VARIÁVEIS ----------------------------------------- #

USUARIOS="$(cat /etc/passwd | cut -d : -f 1)"
MENSAGEM_USO="
    $(basename $0) - [OPÇÕES]

    -h - Menu de Ajuda
    -v - Versão
    -s - Ordenar a saída
    -m - Coloca em maiúsculo
"
VERSAO="v1.0"
CHAVE_ORDENA=0
CHAVE_MAIUSCULO=0

# ------------------------------------------------------------------------ #

# ------------------------------- TESTES ----------------------------------------- #

# ------------------------------------------------------------------------ #

# ------------------------------- EXECUÇÃO ----------------------------------------- #
while test -n "$1"
do
    case "$1" in
        -h) echo "$MENSAGEM_USO" && exit 0               ;;
        -v) echo "$VERSAO" && exit 0                     ;;
        -s) CHAVE_ORDENA=1                               ;;
        -m) CHAVE_MAIUSCULO=1                            ;;
         *) echo "Opção inválida, valie o -h." && exit 1 ;;
    esac
    shift
done

[ $CHAVE_ORDENA -eq 1 ]    && USUARIOS=$(echo "$USUARIOS" | sort)
[ $CHAVE_MAIUSCULO -eq 1 ] && USUARIOS=$(echo "$USUARIOS" | tr [a-z] [A-Z])

echo "$USUARIOS"
# ------------------------------------------------------------------------ #
